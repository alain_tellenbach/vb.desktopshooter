﻿Public Class Form1
    Dim i As Integer = 10
    Public Punkte As Double
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        End
    End Sub

    Private Sub Form1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Click
        My.Computer.Audio.Play(My.Resources.sound, AudioPlayMode.Background)
        Dim einschussloch As New PictureBox
        einschussloch.Height = 64
        einschussloch.Width = 64
        einschussloch.Top = MousePosition.Y
        einschussloch.Left = MousePosition.X
        einschussloch.BackColor = Color.Transparent
        einschussloch.Visible = True
        einschussloch.BackgroundImage = My.Resources.schussloch
        Controls.Add(einschussloch)
        Punkte = Punkte - 2
        Label1.Text = Punkte.ToString
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Opacity = 0
        Dim picture As New Bitmap(My.Computer.Screen.Bounds.Width, My.Computer.Screen.Bounds.Width)
        Dim graphics As Graphics = graphics.FromImage(picture)
        graphics.CopyFromScreen(0, 0, 0, 0, picture.Size)
        Me.BackgroundImage = picture
        Me.Opacity = 1
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        My.Computer.Audio.Play(My.Resources.sound, AudioPlayMode.Background)
        Dim beinschussloch As New PictureBox
        beinschussloch.Height = 64
        beinschussloch.Width = 64
        beinschussloch.Top = PictureBox1.Top
        beinschussloch.Left = PictureBox1.Left
        beinschussloch.BackColor = Color.Transparent
        beinschussloch.Visible = True
        beinschussloch.BackgroundImage = My.Resources.blut
        PictureBox1.Visible = False
        Controls.Add(beinschussloch)
        Punkte = Punkte + 1
        Label1.Text = Punkte.ToString

        Dim rnd As New System.Random
        Dim x As Integer = rnd.Next(0, Me.Width)
        Dim y As Integer = rnd.Next(0, Me.Height)
        PictureBox1.Left = x
        PictureBox1.Top = y
        PictureBox1.Visible = True
    End Sub

    Private Sub Form1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown
        timer1.start()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If i = 0 Then
            My.Computer.Audio.Play(My.Resources.sound, AudioPlayMode.Background)
            Dim einschussloch As New PictureBox
            einschussloch.Height = 32
            einschussloch.Width = 32
            einschussloch.Top = MousePosition.Y
            einschussloch.Left = MousePosition.X
            einschussloch.BackColor = Color.Transparent
            einschussloch.Visible = True
            einschussloch.BackgroundImage = My.Resources.maschine
            Controls.Add(einschussloch)
        Else
            i -= 1
        End If
    End Sub

    Private Sub Form1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        Timer1.Stop()
        i = 10
    End Sub
End Class